package pw.bowser.libhysteria.util.scala.json
import scala.util.parsing.json._

/**
 * Date: 2/10/14
 * Time: 12:54 PM
 */
object Formatters {

  /*
   * Much thanks to https://gist.github.com/aloiscochard/1278959
   */

  val prettyFormatter: JSONFormat.ValueFormatter = {
    def create(level: Int): JSONFormat.ValueFormatter = {
      case s: String => "\"" + JSONFormat.quoteString(s) + "\""
      case jo: JSONObject =>
        "{\n" +
          "\t" * (level + 1) +
          jo.obj.map({
            case (k, v) => JSONFormat.defaultFormatter(k.toString) + ": " + create(level + 1)(v)
          }).mkString(",\n" +
            "\t" * (level + 1)) +
          "\n" +
          "\t" * level +
          "}"
      case ja: JSONArray => ja.toString(create(level))
      case other => other.toString
    }
    create(0)
  }

}
