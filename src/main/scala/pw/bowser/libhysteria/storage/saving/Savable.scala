package pw.bowser.libhysteria.storage.saving

/**
 * Something that needs save/load service
 *
 * Date: 2/1/14
 * Time: 7:21 PM
 */
trait Savable {

  /**
   * Save the Savable
   */
  def save()

  /**
   * Load the Savable
   */
  def load()

}
