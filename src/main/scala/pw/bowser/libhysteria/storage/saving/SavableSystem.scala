package pw.bowser.libhysteria.storage.saving

import scala.collection.mutable

/**
 * Central collection of `Saving`s that may be saved/loaded en mass
 *
 * Date: 2/1/14
 * Time: 7:22 PM
 */
class SavableSystem extends Savable {

  private val savings: mutable.HashSet[Savable] = new mutable.HashSet[Savable]()

  private val saveErrorCallbacks = new mutable.HashSet[((Savable, Throwable) => Unit)]()
  private val loadErrorCallbacks = new mutable.HashSet[((Savable, Throwable) => Unit)]()

  /**
   * Register a callback to receive errors that occur when saving a `Saving`
   * @param callback callback
   */
  def onSaveError(callback: (Savable, Throwable) => Unit): Unit = saveErrorCallbacks += callback

  /**
   * Register a callback to receive errors that occur when loading a `Saving`
   * @param callback callback
   */
  def onLoadError(callback: (Savable, Throwable) => Unit): Unit = loadErrorCallbacks += callback

  /**
   * Enroll a `Saving`
   * @param saving  saving
   */
  def enroll    (saving: Savable): Unit  = savings.add(saving)

  /**
   * Disenroll a `Saving`
   * @param saving  saving
   */
  def disenroll (saving: Savable): Unit  = savings.remove(saving)

  override def save(): Unit = savings.foreach(s =>
    try s.save() catch {case(t: Throwable) => saveErrorCallbacks.foreach(_(s, t))}
  )

  override def load(): Unit = savings.foreach(s =>
    try s.load() catch {case(t: Throwable) => loadErrorCallbacks.foreach(_(s, t))}
  )

}