package pw.bowser.libhysteria.storage.marshal

import java.io.{InputStream, OutputStream}

/**
 * Reads and writes self to an output stream
 *
 * Date: 1/26/14
 * Time: 11:02 PM
 */
trait Marshalling {

  /**
   * Marshal self to output stream
   *
   * @param stream stream to write to
   */
  def writeTo(stream: OutputStream): Unit

  /**
   * Load self from input stream
   *
   * @param stream stream to read from
   */
  def readFrom(stream: InputStream): Unit

  /**
   * Get the handle of the marshallable (for ref like filename etc)
   *
   * @return handle
   */
  def getHandle: String

}
