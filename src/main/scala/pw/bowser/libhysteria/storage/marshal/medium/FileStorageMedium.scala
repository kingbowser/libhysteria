package pw.bowser.libhysteria.storage.marshal.medium

import java.io.{FileInputStream, FileOutputStream, IOException, File}
import pw.bowser.libhysteria.storage.marshal.Marshalling

/**
 * Date: 1/29/14
 * Time: 8:56 AM
 *
 *
 */
class FileStorageMedium(val file: File) extends StorageMedium {

  // Check that the file exists
  if(!file.exists){
    file.createNewFile()
  }

  if(file.isDirectory){
    throw new IOException(s"Specified file for storage medium (${file.getAbsolutePath}) is a directory.")
  }


  /**
   * Marshal the specified Marshalling implementation to the medium
   */
  def save(m: Marshalling): Unit = {
    val fos: FileOutputStream = new FileOutputStream(file)
    m.writeTo(fos)
    fos.flush()
    fos.close()
  }

  /**
   * Load the specified Marshalling from the medium
   */
  def load(m: Marshalling): Unit = {
    val fis: FileInputStream = new FileInputStream(file)
    m.readFrom(fis)
  }

  /**
   * Delete the data in the storage medium
   *
   */
  def deleteData(): Unit = {
    file.delete()
    file.createNewFile()
  }

  override def toString: String = s"FileMedium{file = ${file.getAbsolutePath}}"
}
