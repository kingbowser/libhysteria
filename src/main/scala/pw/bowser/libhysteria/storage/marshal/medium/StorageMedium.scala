package pw.bowser.libhysteria.storage.marshal.medium

import pw.bowser.libhysteria.storage.marshal.Marshalling

/**
 * Specifies storage media on to which a `Marshalling` may be saved and from which a Marshalling may also be loaded
 *
 * Date: 1/26/14
 * Time: 11:05 PM
 */
trait StorageMedium {

  /**
   * Marshal the specified Marshalling implementation to the medium
   */
  def save(m: Marshalling): Unit

  /**
   * Load the specified Marshalling from the medium
   */
  def load(m: Marshalling): Unit

  /**
   * Delete the data in the storage medium
   *
   */
  def deleteData()

}
