package pw.bowser.libhysteria.storage.marshal.registrar

import scala.collection.mutable
import pw.bowser.libhysteria.storage.marshal.Marshalling
import pw.bowser.libhysteria.storage.marshal.medium.StorageMedium
import pw.bowser.libhysteria.storage.saving.Savable

/**
 * Handles a quantity of storage services in a single folder
 *
 * Date: 1/28/14
 * Time: 3:54 PM
 */
abstract class StorageService extends Savable {

  private val mediaMap: mutable.HashMap[StorageMedium, Marshalling] = new mutable.HashMap[StorageMedium, Marshalling]()

  /**
   * Get an appropriate storage medium for the marshallable
   * @param source
   * @return
   */
  def getMediumFor(source: Marshalling): StorageMedium

  /**
   * Register a medium that will receive data from a a marshalling
   *
   * @param dataSource  data source
   */
  final def enroll(dataSource: Marshalling): StorageMedium = {

    if(mediaMap.exists( pair => pair._2 == dataSource ))
      throw new IllegalArgumentException("Data source already registered")

    val medium: StorageMedium = getMediumFor(dataSource)

    mediaMap.put(medium, dataSource)

    medium

  }

  /**
   * Disconnect a media service client
   *
   * @param medium client
   */
  final def disenroll(medium: Marshalling) {
    mediaMap.dropWhile(pair => pair._2 == medium)
  }

  /**
   * Save all data sources to their respective media
   */
  final def save() {
    mediaMap.keySet.foreach({
      medium =>
        try saveMediaTo(medium) catch {
          case t: Throwable =>
            System.err.println(s"$medium could not be saved") // TODO Should savable have its own callback for cases like this?
            t.printStackTrace()
        }
    })
  }

  /**
   * Load all data sources from their respective media
   */
  final def load() {
    mediaMap.keySet.foreach({
      medium =>
        try loadMediaFrom(medium) catch {
          case t: Throwable =>
            System.err.println(s"$medium could not be loaded")
            t.printStackTrace()
        }
    })
  }

  /**
   * Get the number of clients
   *
   * @return client count
   */
  final def size: Int = mediaMap.size

  /**
   * Load the corresponding data source
   *
   * @param medium data storage medium
   */
  def loadMediaFrom(medium: StorageMedium){
    medium.load(mediaMap(medium))
  }

  /**
   * Save data from the corresponding data source
   *
   * @param medium data storage medium
   */
  def saveMediaTo(medium: StorageMedium){
    medium.save(mediaMap(medium))
  }

}
