package pw.bowser.libhysteria.storage.marshal.medium

import java.io.{ByteArrayInputStream, ByteArrayOutputStream}
import pw.bowser.libhysteria.storage.marshal.Marshalling

/**
 * Transient storage medium that stores Marshal data in a byte array
 *
 * Date: 1/26/14
 * Time: 11:13 PM
 */
class MemoryStorageMedium extends StorageMedium {

  private var data: Array[Byte] = Array[Byte](0)

  /**
   * Marshal the specified Marshalling implementation to the medium
   */
  def save(marshalling: Marshalling): Unit = {
    val baos: ByteArrayOutputStream = new ByteArrayOutputStream()
    marshalling.writeTo(baos)
    baos.flush()
    baos.close()
    data = baos.toByteArray
  }

  /**
   * Load the specified Marshalling from the medium
   */
  def load(marshalling: Marshalling): Unit = {
    val bais: ByteArrayInputStream = new ByteArrayInputStream(data)
    marshalling.readFrom(bais)
    bais.close()
  }

  /**
   * Delete the data in the storage medium
   *
   */
  def deleteData(): Unit = data = Array[Byte](0)

  def bytes: Array[Byte] = data

}
