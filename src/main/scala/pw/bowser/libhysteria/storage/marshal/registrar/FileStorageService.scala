package pw.bowser.libhysteria.storage.marshal.registrar

import java.io.File
import pw.bowser.libhysteria.storage.marshal.Marshalling
import pw.bowser.libhysteria.storage.marshal.medium.{StorageMedium, FileStorageMedium}

/**
 * Date: 1/29/14
 * Time: 8:54 PM
 */
class FileStorageService(val storageFolder: File, val fileSuffix: Option[String] = Some("dat")) extends StorageService {

  /**
   * Get an appropriate storage medium for the marshallable
   * @param source
   * @return
   */
  def getMediumFor(source: Marshalling): StorageMedium = {
    new FileStorageMedium(new File(storageFolder, s"${source.getHandle}" + (if(fileSuffix != None) "." + fileSuffix.get else "")))
  }

}
