package pw.bowser.libhysteria.config.map

import pw.bowser.libhysteria.config.Configuration
import java.io.{OutputStream, InputStream}
import scala.collection.mutable.{Map => MutableMap}
import scala.util.parsing.json.{JSON, JSONObject}
import pw.bowser.libhysteria.util.scala.json.Formatters
import scala.io.Source

/**
 * Date: 2/24/14
 * Time: 9:39 AM
 */
class MapConfiguration(override val handle: String = "config") extends Configuration(handle) {

  private val properties = MutableMap[String, Any]()

  /**
   * Set a property.
   *
   * @param path  node path
   * @param value value
   */
  def setProperty(path: String, value: Any): Unit = properties.put(path, value)

  /**
   * Unset a property
   *
   * @param path  node path
   */
  def rmProperty(path: String): Unit = properties.remove(path)

  /**
   * Get something from the configuration
   *
   * @param path    Value name
   * @param cType   Type of object
   * @tparam T      Type of object
   * @return value
   */
  def getProperty[T](path: String, cType: Class[T]): T = cType.cast(properties(path))

  /**
   * Whether or not the configuration has the property
   *
   * @param path property exists
   */
  def hasProperty(path: String): Boolean = properties.contains(path)

  /**
   * Marshal self to output stream
   *
   * @param stream stream to write to
   */
  def writeTo(stream: OutputStream): Unit = {
    val jsonObj = JSONObject(properties.toMap)
    stream.write(Formatters.prettyFormatter(jsonObj).getBytes)
  }

  /**
   * Load self from input stream
   *
   * @param stream stream to read from
   */
  def readFrom(stream: InputStream): Unit = {
    val streamContents = Source.createBufferedSource(stream).getLines().mkString("\n")
    val rawJson = JSON.parseRaw(streamContents)

    if (rawJson != None && rawJson.get.isInstanceOf[JSONObject]) {
      val mapFrom = rawJson.get.asInstanceOf[JSONObject].obj
      mapFrom.foreach({
        case (key, value) => properties.put(key, value)
      })
    } else throw new IllegalStateException("Read invalid json data: " + rawJson)
  }

  override def getFloat(path: String): Float = {
    val prop = getProperty(path)
    if (prop.isInstanceOf[Double]) {
      val propNew = prop.asInstanceOf[Double].toFloat
      setProperty(path, propNew)
      propNew
    } else {
      prop.asInstanceOf[Float]
    }
  }

  override def getLong(path: String): Long = {
    val prop = getProperty(path)
    if (prop.isInstanceOf[Double]) {
      val propNew = prop.asInstanceOf[Double].toLong
      setProperty(path, propNew)
      propNew
    } else {
      prop.asInstanceOf[Long]
    }
  }

  override def getInt(path: String): Int = {
    val prop = getProperty(path)
    if (prop.isInstanceOf[Double]) {
      val propNew = prop.asInstanceOf[Double].toInt
      setProperty(path, propNew)
      propNew
    } else {
      prop.asInstanceOf[Int]
    }
  }

  override def getShort(path: String): Short = {
    val prop = getProperty(path)
    if (prop.isInstanceOf[Double]) {
      val propNew = prop.asInstanceOf[Double].toShort
      setProperty(path, propNew)
      propNew
    } else {
      prop.asInstanceOf[Short]
    }
  }

  override def getByte(path: String): Byte = {
    val prop = getProperty(path)
    if (prop.isInstanceOf[Double]) {
      val propNew = prop.asInstanceOf[Double].toByte
      setProperty(path, propNew)
      propNew
    } else {
      prop.asInstanceOf[Byte]
    }
  }


}
