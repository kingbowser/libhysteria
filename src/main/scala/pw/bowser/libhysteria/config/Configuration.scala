package pw.bowser.libhysteria.config

import java.io.{InputStream, OutputStream}
import pw.bowser.libhysteria.storage.marshal.Marshalling

/**
 * Date: 1/19/14
 * Time: 12:11 PM
 */
abstract class Configuration(val handle: String) extends Marshalling {

  /**
   * Set a property.
   *
   * @param path  node path
   * @param value value
   */
  def setProperty(path: String, value: Any)

  /**
   * Set a property only if it did not exist
   *
   * @param path  path to property
   * @param value property value
   */
  final def setPropertyIfNew(path: String, value: Any) {
    if(!hasProperty(path)) setProperty(path, value)
  }

  /**
   * Unset a property
   *
   * @param path  node path
   */
  def rmProperty(path: String)

  /**
   * Get something from the configuration
   *
   * @param path    Value name
   * @param cType   Type of object
   * @tparam T      Type of object
   * @return value
   */
  def getProperty[T](path: String, cType: Class[T] = classOf[java.lang.Object]): T

  /**
   * Whether or not the configuration has the property
   *
   * @param path property exists
   */
  def hasProperty(path: String): Boolean


  /*
   * Instead of implementing a second interface for configuration storage, we can ask the configuration to save itself
   */

  final def getHandle: String = handle

  /*
   * Shorthand access methods for common/literal types in the language
   */

  def getString(path: String): String = getProperty(path, classOf[String])

  // Java number types like to misbehave

  def getByte(path: String): Byte       = getProperty(path, classOf[java.lang.Byte])

  def getShort(path: String): Short     = getProperty(path, classOf[java.lang.Short])

  def getInt(path: String): Int         = getProperty(path, classOf[java.lang.Integer])

  def getLong(path: String): Long       = getProperty(path, classOf[java.lang.Long])

  def getFloat(path: String): Float     = getProperty(path, classOf[java.lang.Float])

  def getDouble(path: String): Double   = getProperty(path, classOf[java.lang.Double])

  def getBoolean(path: String): Boolean = getProperty(path, classOf[java.lang.Boolean])

}
