package pw.bowser.libhysteria.config

/**
 * Exception that should be used when an error occurs in a configuration implementation while working with contents.
 *
 * @param reason  what happened (description)
 * @param key     key that was being queried at exception time
 */
class ConfigurationException(val reason: String, val key: String) extends Exception(s"Unable to retrieve key $key: $reason")
