package pw.bowser.libhysteria.toggles

import scala.collection.mutable
import pw.bowser.libhysteria.storage.marshal.Marshalling
import java.io.{OutputStream, InputStream}
import scala.util.parsing.json.{JSON, JSONObject}
import scala.io.Source
import pw.bowser.libhysteria.util.scala.json.Formatters

/**
 * Manages toggleable persistence and dependency networks. Thus, if you enable a toggleable is stays on the next time you run the client.
 *
 * Date: 2/10/14
 * Time: 9:28 AM
 */
final class ToggleService extends Marshalling {

  private val toggleables     = mutable.Map[String, Toggles]()

  /**
   * Recursive function that resolves toggle dependencies
   *
   * @param toggles toggle for which to resolve dependencies
   * @return        dependencies
   * @throws  IllegalStateException when there is a missing or misspelled dependency
   */
  def resolveNeeds(toggles: Toggles): List[Toggles] = try {

    // Recursively resolve required toggles'

    val togglesRequired     = toggles.requiredToggles
                                .map(toggleables)
                                .map(ts => (ts, resolveNeeds(ts)))


    // Compile list of conflicts and list of requires

    val bufferRequired = mutable.ListBuffer[Toggles]()

    togglesRequired.foreach({pair =>
      bufferRequired +=   pair._1
      bufferRequired ++=  pair._2
    })

    bufferRequired.distinct.toList

  } catch {
    case nse: NoSuchElementException =>
      throw new IllegalStateException(s"$toggles is missing (or has misspelled) dependencies (${nse.getMessage}).", nse)
  }

  /**
   * Enroll a toggleable.
   *
   * This will not trigger a dependency check.
   *
   * @param toggle to enroll
   */
  def enrollToggleable(toggle: Toggles): Unit = toggleables.put(toggle.distinguishedName, toggle)

  /**
   * Disenroll a toggleable
   *
   * This will not trigger a dependency check.
   *
   * @param toggle toggleable
   */
  def disenrollToggleable(toggle: Toggles): Unit = disenrollToggleable(toggle.distinguishedName)

  /**
   * Disenroll a toggleable from the service by the name
   *
   * @param name name of the toggleable
   */
  def disenrollToggleable(name: String): Unit = toggleables.remove(name)

  /**
   * Flip the state of a `Toggles`
   *
   * @param name  toggle name
   * @return      toggle state
   */
  def toggle(name: String): ToggleState = toggle(toggleables(name))

  /**
   * Flip the state of a `Toggles`
   *
   * @param toggles toggles instance
   * @return        toggles state
   */
  def toggle(toggles: Toggles): ToggleState = toggles.getState match {
    case ToggleState.Enabled =>
      setStateOf(toggles, ToggleState.Disabled)
      toggles.getState

    case ToggleState.Disabled | _ =>
      setStateOf(toggles, ToggleState.Enabled)
      toggles.getState

    case _ =>
      /*
       * Do nothing because the state was set automatically
       */

      toggles.getState
  }

  /**
   * Set the state of a `Toggles` to the specified state.
   *
   * Depending on the provided state, one of the following will happen.
   *
   *  Enabled | Required:
   *    - All conflicts will be suppressed (1-deep).
   *    - All requirements will be required (using `ToggleService#resolveNeeds`)
   *
   *  Disabled:
   *    - All conflicts not conflicting with other Toggles will be re-enabled
   *    - All requirements not needed elsewhere will be disabled.
   *
   * @param toggles Toggles to set state for
   * @param toState State to set
   */
  def setStateOf(toggles: Toggles, toState: ToggleState): Unit = toState match {
    case ToggleState.Enabled  | ToggleState.Required    =>
      /*
       * In the case the the toggle is being enabled or required, we need to check the toggles dependencies and conflicts
       *  and enable or disable them as needed
       */

      // Resolve dependencies
      val requirements  = resolveNeeds(toggles)
      val conflicts     = toggles.conflictingToggles.filter(toggleables.contains).map(toggleables)

      // Suppress/Require
      conflicts.foreach {t =>
        if(t.getState == ToggleState.Enabled)   t.setState(ToggleState.Suppressed)
      }

      requirements.foreach {t =>
        if(t.getState == ToggleState.Disabled)  t.setState(ToggleState.Required)
      }

      toggles.setState(toState)



    case ToggleState.Disabled =>
      /*
       * When we disable a toggle (suppression does not count here) we need to check that all dependencies that have
       *  been Required and are not Required elsewhere are not enabled.
       * Any conflicts that are not conflicting with anything else and are suppressed must be re-enabled
       */

      val requirements  = resolveNeeds(toggles)
      val conflicts     = toggles.conflictingToggles.filter(toggleables.contains).map(toggleables)

      conflicts.foreach {t =>
        if(t.getState == ToggleState.Suppressed){
          // Search current conflicts
          val suppressors = toggleables.filterNot(_._2 == toggles).filter({s => s._2.conflictingToggles.contains(t.distinguishedName) && s._2.isEnabled})
          if(suppressors.isEmpty) setStateOf(t, ToggleState.Enabled)
        }
      }

      requirements.foreach {t =>
        if(t.getState == ToggleState.Required){
          // Search for dependants
          val dependants = toggleables.filterNot(_._2 == toggles).filter({s => resolveNeeds(s._2).contains(t) && s._2.isEnabled})
          if(dependants.isEmpty) setStateOf(t, ToggleState.Disabled)
        }
      }

      toggles.setState(toState)

      /*
       * This should also disable any dependants upon the toggles being disabled.
       * Note that this is called after the state is set -- this prevents us from being in recursion hell.
       */

      toggleables.values.filter(_.requiredToggles.contains(toggles.distinguishedName)).foreach {t => if(t.isEnabled) setStateOf(t, ToggleState.Disabled)}

    case _ => toggles.setState(toState)
  }

  /**
   * Set the state of a toggle by distinguished name
   *
   * @param distName  distinguished name
   * @param state     state
   * @throws IllegalArgumentException when it is unable to look up the distinguished name in the registry
   * @see ToggleService#setStateOf(Toggles, ToggleState)
   * */
  def setStateOf(distName: String, state: ToggleState): Unit = try {
    setStateOf(toggleables(distName), state)
  } catch {
    case nse: NoSuchElementException =>
      throw new IllegalArgumentException(s"Toggle $distName is not enrolled", nse)
  }


  /**
   * Returns the state of a `Toggles` named by the provided distinguished name.
   *
   * This will look up the toggle and call `Toggles#getState` on it.
   *
   * @param name distinguished name
   * @return     state
   */
  def getStateOf(name: String): ToggleState = toggleables(name).getState

  /**
   * Returns all enrolled `Toggles`
   *
   * @return enrolled `Toggles`
   */
  def enrolledToggles: Iterable[Toggles] = toggleables.values

  /**
   * Write the state of all toggles to a stream as JSON data.
   *
   * The data looks like this (example):
   *
   * <code>
   *  {
   *    "pw.bowser.Toggle": "Enabled"
   *  }
   * </code>
   *
   * @param stream stream to write to
   */
  def writeTo(stream: OutputStream): Unit = {

    val states = toggleables.values.map(t => t.getState.name)
    val mapped = (toggleables.keys zip states).toMap

    stream.write(Formatters.prettyFormatter(JSONObject(mapped)).getBytes)

  }

  /**
   * Load the state of toggles from an input stream
   *
   * Once the data is decoded, all toggles will be disabled regardless of state, and toggles that were enabled will be
   *  placed in a buffer.
   * This buffer is then used to enable all toggles that should be disabled.
   *
   * This works because any other state will either be determined by the dependency mechanism or be disabled.
   *
   * @param stream stream to read from
   */
  def readFrom(stream: InputStream): Unit = {

    val data      = Source.fromInputStream(stream).getLines().mkString("\n")
    val json      = JSON.parseRaw(data).get

    val root      = json.asInstanceOf[JSONObject]

    val toEnable  = mutable.ListBuffer[Toggles]()

    root.obj.foreach({
      state =>
        try {
          val tog       = toggleables(state._1)
          val togState  = ToggleState(state._2.asInstanceOf[String])

          if(tog.canPersist && togState == ToggleState.Enabled) toEnable += tog
          tog.setState(ToggleState.Disabled)
        } catch {
          case e: Throwable => // Something something error handling TODO
        }
    })

    toEnable.foreach(setStateOf(_, ToggleState.Enabled))

  }

  def getHandle: String = "toggle-states"
}