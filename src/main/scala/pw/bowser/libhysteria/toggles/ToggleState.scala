package pw.bowser.libhysteria.toggles


/**
 * Describes the state of a toggleable
 * Date: 2/10/14
 * Time: 9:33 AM
 */
final case class ToggleState(name: String)

object ToggleState {

  val Enabled     = ToggleState("enabled")
  val Disabled    = ToggleState("disabled")
  val Required    = ToggleState("required")
  val Suppressed  = ToggleState("suppressed")

}