package pw.bowser.libhysteria.toggles

import java.util.Properties

/**
 * A "Toggleable" -- something like NoFall, an AutoBlow 3000, or the toaster, for instance.
 * Toggleable have two states that are persisted in configuration, they also support attributes, like colours.
 *
 * Date: 2/10/14
 * Time: 9:30 AM
 */
trait Toggles {

  /**
   * True if the state of the Toggleable can be saved and loaded
   * @return can be saved
   */
  def canPersist: Boolean

  /**
   * Should return the distinguished name of the toggles
   * The distinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
   * @return
   */
  def distinguishedName: String

  /**
   * Get the name of the toggleable
   *
   * @return name
   */
  def displayName: String

  /**
   * Get the attributes that belong to the toggleable
   *
   * @return attributes
   */
  def getAttributes: Option[Properties]

  /**
   * Get the state of the toggleable
   *
   * @return state
   */
  def getState: ToggleState

  /**
   * Set the state of the toggles
   *
   * @param stateNew new state
   */
  def setState(stateNew: ToggleState): Unit

  /**
   * Get whether or not the toggles is enabled
   *
   * @return state
   */
  def isEnabled: Boolean

  /**
   * Status text of the Toggles
   *
   * @return status text, none if none
   */
  def statusText: Option[String]

  /*
   * Dependency system
   */

  /**
   * List of toggles by distinguished name that conflict with this toggle's operation
   *
   * @return conflicting toggles
   */
  def conflictingToggles: List[String]

  /**
   * List of toggles by distinguished name that are need for the toggle to work
   *
   * @return required toggles
   */
  def requiredToggles: List[String]

}

/**
 * Create toggleable's otf
 */
object Toggles {

  /**
   * Create an anonymous toggleable otf
   *
   * @param distName            name of the toggleable
   * @param dispName            display name of the toggleable
   * @param onEnableClosure     action to perform on enable
   * @param onDisableClosure    action to perform on disable
   * @param persists            should the toggleable be saved?
   * @return                    toggleable
   */
  def apply(distName: String, dispName: String,
            onEnableClosure: => Unit, onDisableClosure: => Unit,
            persists: Boolean,
            requires: List[String], conflicts: List[String]): Toggles = {
    new TogglesMixin {

      /**
       * Get the name of the toggleable
       *
       * @return name
       */
      def displayName: String = dispName

      /**
       * Shoudl return the distinguished name of the toggles
       * The disinguished name should be unique to the toggles, ie, `pw.bowser.mods.mod.distinguished_name_here`
       * @return
       */
      override def distinguishedName: String = distName

      /**
       * Fork method that is called when the toggleable is disabled
       */
      override def enable(): Unit = {
        onEnableClosure
      }

      /**
       * Fork method that is called when the toggleable is enabled
       */
      override def disable(): Unit = {
        onDisableClosure
      }

      /**
       * True if the state of the Toggleable can be saved and loaded
       * @return can be saved
       */
      override def canPersist: Boolean = persists

      override def conflictingToggles: List[String] = conflicts

      override def requiredToggles: List[String] = requires

    }
  }

  /**
   * Create an anonymous toggleable otf that persists
   *
   * @param name              name of the toggleable
   * @param dName             display name
   * @param onEnableClosure   action to perform on enable
   * @param onDisableClosure  action to perform on disable
   * @return                  toggleable
   */
  def apply(name: String, dName: String,
            onEnableClosure: => Unit, onDisableClosure: => Unit,
            requires: List[String], conflicts: List[String]): Toggles =
    apply(distName = name, dispName = dName,
          onEnableClosure = onEnableClosure, onDisableClosure = onDisableClosure,
          persists = true, requires = requires, conflicts = conflicts)

  /**
   * Create a toggles that has no dependencies
   *
   * @param name              distinguished name
   * @param dName             display name
   * @param onEnableClojure   action on enable
   * @param onDisableClosure  action on disable
   * @return                  toggles
   */
  def apply(name: String, dName: String,
            onEnableClojure: => Unit, onDisableClosure: => Unit): Toggles = apply(name, dName, onEnableClojure, onDisableClosure, List(), List())

  /**
   * Create a toggles that has no action
   *
   * @param name        distinguished name
   * @param dName       display name
   * @param requires    dependencies
   * @param conflicts   conflicts
   * @return            toggles
   */
  def apply(name: String, dName: String,
            requires: List[String], conflicts: List[String]): Toggles = apply(name, dName, {}, {}, List(), List())

  /**
   * Create a toggles that has no action or dependencies
   *
   * @param name    distinguished name
   * @param dName   display name
   * @return        toggles
   */
  def apply(name: String, dName: String): Toggles = apply(name, dName, {}, {})

}

/**
 * Mixin that allows for easy implementation of the `Toggles` trait.
 */
trait TogglesMixin extends Toggles {

  private var state: ToggleState = ToggleState.Disabled

  /**
   * Called when the toggles is enabled or required
   * The implementation should store logic to start and enable functionality here.
   */
  def enable(): Unit = {}

  /**
   * Called when the toggles is disabled or suppressed.
   * The implementation should store logic to halt and disable functionality here.
   */
  def disable(): Unit = {}

  final def setState(nState: ToggleState): Unit = nState match {
    case ToggleState.Enabled  | ToggleState.Required    =>
      enable()
      state = nState
    case ToggleState.Disabled | ToggleState.Suppressed  =>
      disable()
      state = nState
  }

  final def getState: ToggleState = state

  def getAttributes: Option[Properties] = None

  def canPersist: Boolean = true

  final def isEnabled: Boolean = getState == ToggleState.Enabled | getState == ToggleState.Required

  def statusText: Option[String] = None

  def conflictingToggles: List[String] = List()

  def requiredToggles: List[String] = List()

  override def toString: String =
    s"TogglesMixin($distinguishedName, $getState)"

}