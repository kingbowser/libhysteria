package pw.bowser.libhysteria.commands

import pw.hysteria.input.dashfoo.command.HelpProvider

/**
 * Provides help based on constructor parameters
 *
 * Date: 2/6/14
 * Time: 6:01 PM
 */
final class SimpleHelpProvider(val help: String, val info: String) extends HelpProvider {
  def getDescription: String = info

  def getHelp: String = help
}
