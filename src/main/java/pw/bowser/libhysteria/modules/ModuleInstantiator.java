package pw.bowser.libhysteria.modules;

/**
 * Provides instantiation logic for module classes
 *
 * Date: 4/28/14
 * Time: 2:05 PM
 */
public interface ModuleInstantiator {

    /**
     * Create an instance of the specified module
     * @param moduleClass module class
     * @param <MT>        module type
     * @return            module instance
     */
    public <MT extends Module> MT getInstance(Class<MT> moduleClass);

}
