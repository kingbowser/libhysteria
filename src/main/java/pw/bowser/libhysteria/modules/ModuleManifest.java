package pw.bowser.libhysteria.modules;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Date: 2/4/14
 * Time: 8:42 AM
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ModuleManifest {

    String groupId();

    String name();

    String version();

    String description();

    String[] commands() default {};

    String[] properties() default {};

}
