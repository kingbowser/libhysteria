package pw.bowser.libhysteria.modules;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

/**
 * Manage module instances and such
 *
 * Date: 1/24/14
 * Time: 9:22 PM
 */
public class ModuleLoader {

    /**
     * Factory the creates an instance from a module type according to implementation-specific logic (IE Guice)
     */
    private final ModuleInstantiator instantiator;

    /**
     * Map of module types to their instances
     */
    private final Map<Class<? extends Module>, Module> instanceMap = new HashMap<>();

    /**
     * Logger used by the loader
     */
    private final Logger  logger;

    /**
     * Root that will contain storage folders for each module
     */
    private final File    rootFolder;

    /**
     * Instantiate the loader
     *
     * @param instantiator          module instantiator
     * @param logger                logger
     * @param rootFolder            location that all module storage is in
     */
    public ModuleLoader(
            File rootFolder,
            ModuleInstantiator instantiator,
            Logger logger){
        this.instantiator   = instantiator;
        this.logger         = logger;
        this.rootFolder     = rootFolder;
        this.logger.setLevel(Level.ALL);
    }

    /**
     * Create a new module instance of the specified module class.
     *
     * @param moduleClass   module class
     * @param <M>           module type
     * @return              module instance
     * @throws pw.bowser.libhysteria.modules.ModuleInitializationException when an error occurs while initializing the module
     */
    @SuppressWarnings("FeatureEnvy" /* ModuleDesc - Annotation params are accessed as method calls. */ )
    public <M extends Module> M loadModule(Class<M> moduleClass) throws ModuleInitializationException {

        if(instanceMap.containsKey(moduleClass)) throw new ModuleInitializationException(null, "Module instance already exists");

        //--------------------------------------------------------------------------------------------------------------

        if(!moduleClass.isAnnotationPresent(ModuleManifest.class))
            throw new ModuleInitializationException(null, "Module class lacks a manifest annotation");

        ModuleManifest desc = moduleClass.getAnnotation(ModuleManifest.class);

        final File moduleStorage            = new File(this.rootFolder, desc.name());

        //--------------------------------------------------------------------------------------------------------------

        logger.info("Loading module " + desc.groupId() + "."  + desc.name());

        M modInstance = instantiator.getInstance(moduleClass);

        /*
         * Create and configure a logger for the module
         */

        Logger pluginLogger = Logger.getLogger("plugin:" + desc.groupId() + "." + desc.name());
        try {
            FileHandler handler = new FileHandler(new File(moduleStorage, "plugin.log").getAbsolutePath(), true);
            handler.setFormatter(new SimpleFormatter());
            handler.setLevel(Level.ALL);
            pluginLogger.addHandler(handler);
            pluginLogger.setUseParentHandlers(false);
        } catch (IOException e) {
            logger.log(Level.SEVERE, "Unable to configure plugin logger file handler for plugin " + desc.groupId() + "." + desc.name(), e);
        }


        /*
         * Configure module
         */

        logger.info("Setting module storage location and logger");

        // Set module's storage folder
        modInstance.setDataFolder(moduleStorage);

        // Store an instance related to the module type (Class)
        instanceMap.put(moduleClass, modInstance);

        // Call module init
        modInstance.initModule();

        //--------------------------------------------------------------------------------------------------------------

        return modInstance;
    }

    /**
     * Get the instance of a module held for the specified class
     *
     * @param moduleClass   module class
     * @param <T>           module type
     * @return              module instance or null
     */
    public <T extends Module> T getModule(Class<T> moduleClass){
        //This class is safe because we can only have inserted the proper module alongside the class
        //noinspection unchecked
        return (T) instanceMap.get(moduleClass);
    }

    /**
     * Disable the module loaded for said class
     *
     * @param moduleClass       module class
     * @throws ModuleException  see {@link pw.bowser.libhysteria.modules.Module#disableModule()}
     */
    public void disableModule(Class<? extends Module> moduleClass) throws ModuleException {
        if(!instanceMap.containsKey(moduleClass)){
            logger.warning("A call was made to disable an unloaded module. " + moduleClass.getName());
            return;
        }

        logger.fine("Disabling " + moduleClass.getName());

        instanceMap.get(moduleClass).disableModule();
    }

    /**
     * Enable the module loaded for said class
     *
     * @param moduleClass       module class
     * @throws ModuleException  see {@link pw.bowser.libhysteria.modules.Module#enableModule()}
     */
    public void enableModule(Class<? extends Module> moduleClass) throws ModuleException {
        if(!instanceMap.containsKey(moduleClass)){
            logger.warning("A call was made to enable an unloaded module. " + moduleClass.getName());
            return;
        }

        logger.fine("Disabling " + moduleClass.getName());

        instanceMap.get(moduleClass).enableModule();
    }

    /**
     * Enable all modules
     *
     * @throws ModuleException if thrown by enableModule(Class)
     */
    public final void enableAll() throws ModuleException {
        for(Class<? extends Module> m : instanceMap.keySet()){
            enableModule(m);
        }
    }

    /**
     * Disable all modules
     *
     * @throws ModuleException if thrown by disableModule(Class)
     */
    public final void disableAll() throws ModuleException {
        for(Class<? extends Module> m : instanceMap.keySet()){
            disableModule(m);
        }
    }

}
