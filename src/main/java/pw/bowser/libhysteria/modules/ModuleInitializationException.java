package pw.bowser.libhysteria.modules;

/**
 * Date: 1/23/14
 * Time: 7:48 PM
 */
public class ModuleInitializationException extends ModuleException {
    public ModuleInitializationException(Module rel) {
        this(rel, "");
    }

    public ModuleInitializationException(Module rel, Throwable cause){
        this(rel, "",cause);
    }

    public ModuleInitializationException(Module rel, String message){
        this(rel, message, null);
    }

    public ModuleInitializationException(Module rel, String msg, Throwable cause) {
        super("Error occurred while initializing the module " + rel.toString() + ": " + msg, cause);
    }
}
