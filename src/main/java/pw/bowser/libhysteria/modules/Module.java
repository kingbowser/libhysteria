package pw.bowser.libhysteria.modules;

import java.io.File;
import java.util.logging.Logger;

/**
 * Date: 1/23/14
 * Time: 6:52 PM
 */
public abstract class Module {

    private File dataFolder             = null;

    private Logger logger               = null;

    private ModuleState state           = ModuleState.STOPPED;

    /*
     * Package interface
     */

    public final void setDataFolder(File folder) {
        if(this.dataFolder != null) throw new IllegalStateException("Data folder has already been set!");
        this.dataFolder = folder;
    }

    public final void setLogger(Logger logger) {
        if(this.logger != null) throw new IllegalStateException("Logger has already been set!");
        this.logger = logger;
    }

    /*
     * Child interface
     */

    /**
     * Get the logger for the module
     *
     * @return logger
     */
    protected final Logger getLogger() {
        if(logger == null) throw new IllegalStateException("Logger has not been initialized");
        return logger;
    }

    /**
     * Folder wherein plugin information may be stored
     *
     * @return folder
     */
    protected final File getPluginDataFolder(){
        if(dataFolder == null) throw new IllegalStateException("Data folder has not been set. Was the module loaded correctly?");
        return dataFolder;
    }

    /**
     * Set the module's state description
     *
     * @param state state
     */
    protected final void setModuleState(ModuleState state) { this.state = state; }

    /*
     * Public interface
     */

    /**
     * The module state
     *
     * @return module state
     */
    public final ModuleState getModuleState() { return state; }

    /*
     * Public abstract interface
     */

    /**
     * Initialize the module
     *
     * @throws ModuleInitializationException if an error occurs during initialization
     */
    public abstract void initModule() throws ModuleInitializationException;

    /**
     * Called preceding the enabling of the module
     *
     * @throws ModuleException if an error occurs while enabling the module
     */
    public abstract void enableModule() throws ModuleException;

    /**
     * Called preceding the disablement of the module
     *
     * @throws ModuleException if an error occurs while disabling the module
     */
    public abstract void disableModule() throws ModuleException;

}
