package pw.bowser.libhysteria.modules;

/**
 * Date: 1/23/14
 * Time: 7:49 PM
 */
public enum ModuleState {

    /**
     * The module is loaded and ready, or is working
     */
    OK,

    /**
     * The module is loaded and disabled
     */
    STOPPED,

    /**
     * The module could not be loaded or encountered an error
     */
    FAIL

}
