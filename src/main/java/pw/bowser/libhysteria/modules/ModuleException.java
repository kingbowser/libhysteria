package pw.bowser.libhysteria.modules;

/**
 *
 * Date: 1/23/14
 * Time: 7:41 PM
 */
public class ModuleException extends Exception {

    private Module moduleRelated = null;

    public ModuleException(Module rel) {
        this(rel, null);
    }

    public ModuleException(Module rel, Throwable cause) {
        this("An exception occurred in module " + rel.toString(), rel, cause);
    }

    public ModuleException(String desc, Module rel, Throwable cause) {
        super(desc, cause);
        this.moduleRelated = rel;
    }

    public ModuleException(String desc, Module rel) {
        this(desc, rel, null);
    }

    public ModuleException(String rel, Throwable cause) {
        this(rel, null, cause);
    }

    public ModuleException(String rel) {
        this(rel, (Throwable) null);
    }

    public Module getModule() {
        return moduleRelated;
    }

}
