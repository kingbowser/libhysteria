package pw.bowser.libhysteria.test

import org.junit.Test
import pw.bowser.libhysteria.config.map.MapConfiguration
import pw.bowser.libhysteria.storage.marshal.medium.MemoryStorageMedium

/**
 * Date: 2/24/14
 * Time: 1:41 PM
 */
class MapConfigurationTest {

  @Test def testStoresTypesProperly() {
    val config = new MapConfiguration
    val media = new MemoryStorageMedium

    config.setProperty("bowser.has.byte", 255 toByte)
    config.setProperty("bowser.has.short", 300 toShort)
    config.setProperty("bowser.has.int", 133)
    config.setProperty("bowser.has.long", 1337L)
    config.setProperty("bowser.has.float", 1.0F)
    config.setProperty("bowser.has.double", 1.0D)
    config.setProperty("bowser.has.string", "I'm sexy")

    media.save(config)
    println(new String(media.bytes))
    media.load(config)

    assert(config.getByte("bowser.has.byte") == (255 toByte))
    assert(config.getShort("bowser.has.short") == (300 toShort))
    assert(config.getInt("bowser.has.int") == 133)
    assert(config.getLong("bowser.has.long") == 1337L)
    assert(config.getFloat("bowser.has.float") == 1.0F)
    assert(config.getDouble("bowser.has.double") == 1.0D)
    assert(config.getString("bowser.has.string") == "I'm sexy")
  }

  @Test def testConfigHasProperty() {
    val config = new MapConfiguration

    config.setProperty("bowser.has.this", true)
    config.setProperty("bowser.not.has.this", false)
    config.rmProperty("bowser.not.has.this")

    assert(config.hasProperty("bowser.has.this"))
    assert(config.hasProperty("bowser.not.has.this") == false)
  }

  @Test def testComplexTypes() {
    val config = new MapConfiguration

    config.setProperty("bowser.has.list", List("men", "food", "money"))

    assert(config.getProperty("bowser.has.list", classOf[List[String]]).isInstanceOf[List[String]])
  }

  @Test def insureThatLoadingStoredConfigurationDoesNotOverwriteNewValues() {

    val c = new MapConfiguration
    val m = new MemoryStorageMedium

    c.setPropertyIfNew("am.irritated", true)

    m.save(c)

    c.setPropertyIfNew("hysteria.fun", true)

    m.load(c)

    assert(c.hasProperty("hysteria.fun"))

    m.save(c)

    println(new String(m.bytes))

  }

}
