package pw.bowser.libhysteria.test

import org.junit.Test
import pw.bowser.libhysteria.toggles.{ToggleState, Toggles, ToggleService}
import scala.collection.mutable
import pw.bowser.libhysteria.storage.marshal.medium.MemoryStorageMedium

/**
 * Date: 5/5/14
 * Time: 11:22 PM
 */
class TogglesSuite {

  /**
   * This test insures that the toggle service handles deeply nested dependency trees well.
   * As it stands, 20-deep is very unreasonable - however, it does perform well.
   * This was tested with 75-deep and it completed in 6 seconds, which is unacceptable, but so is that sort of nesting.
   */
  @Test def resolvesLinearNests(): Unit = {

    // In order to work
    // The full dependency tree of a toggles
    // Must resolve to a flat list

    val toggleSystem = new ToggleService

    // Generate a shit-tonne of toggles
    val togglesBuffer = mutable.ListBuffer[Toggles]()

    for(i <- 0 to 20) {
      togglesBuffer += Toggles(s"pw.bowser.$i", s"$i", {}, {}, true, if (i > 0) List(s"pw.bowser.${i - 1}") else List(), List())
    }

    togglesBuffer.foreach(toggleSystem.enrollToggleable)

    val toggleC = Toggles("pw.bowser.test.tip", "tip", {}, {}, true, List(togglesBuffer.last.distinguishedName), List())
    toggleSystem.enrollToggleable(toggleC)

    // Enable tip

    toggleSystem.setStateOf(toggleC, ToggleState.Enabled)

    assert(togglesBuffer.forall(_.isEnabled))

    // Disable tip

    toggleSystem.setStateOf(toggleC, ToggleState.Disabled)

    assert(togglesBuffer.forall(!_.isEnabled))

  }

  /**
   * This test insures that the toggle service resolves non or para linear trees, where one branch might have more than one
   *  dependency.
   */
  @Test def resolvesOrganicNests(): Unit = {

    val toggleSystem = new ToggleService

    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.tip", s"Tip",   {}, {}, true,   List("pw.bowser.a", "pw.bowser.c"), List()))
    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.a", s"Iceberg", {}, {}, true, List("pw.bowser.b", "pw.bowser.d"), List()))
    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.b", s"Iceberg", {}, {}, true, List("pw.bowser.f", "pw.bowser.d"), List()))
    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.c", s"Iceberg", {}, {}, true, List("pw.bowser.e"), List()))
    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.d", s"Iceberg", {}, {}, true, List(), List()))
    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.e", s"Iceberg", {}, {}, true, List("pw.bowser.f"), List()))
    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.f", s"Iceberg", {}, {}, true, List(), List()))

    toggleSystem.setStateOf("pw.bowser.tip", ToggleState.Enabled)

    assert(toggleSystem.enrolledToggles.forall(_.isEnabled))

  }

  /**
   * This test insures that the toggle service will disable any dependant if it has a child disabled
   */
  @Test def disablingDependencyDisablesParent(): Unit = {

    val toggleSystem = new ToggleService

    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.tip", s"Tip",     {}, {}, true, List("pw.bowser.a"), List()))
    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.a",   s"Iceberg", {}, {}, true, List(), List()))

    // Enable tip (thus enabling a)

    toggleSystem.setStateOf("pw.bowser.tip", ToggleState.Enabled)

    assert(toggleSystem.enrolledToggles.forall(_.isEnabled))

    // Disable a (this should disable tip)

    toggleSystem.setStateOf("pw.bowser.a", ToggleState.Disabled)

    assert(toggleSystem.enrolledToggles.forall(!_.isEnabled))

  }

  @Test def serviceCanPersistState(): Unit = {

    val toggleSystem = new ToggleService

    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.tip", s"Tip",   {}, {}, true,   List("pw.bowser.a", "pw.bowser.c"), List()))
    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.a", s"Iceberg", {}, {}, true, List("pw.bowser.b", "pw.bowser.d"), List()))
    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.b", s"Iceberg", {}, {}, true, List("pw.bowser.f", "pw.bowser.d"), List()))
    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.c", s"Iceberg", {}, {}, true, List("pw.bowser.e"), List()))
    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.d", s"Iceberg", {}, {}, true, List(), List()))
    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.e", s"Iceberg", {}, {}, true, List("pw.bowser.f"), List()))
    toggleSystem.enrollToggleable(Toggles(s"pw.bowser.f", s"Iceberg", {}, {}, true, List(), List()))


    toggleSystem.setStateOf("pw.bowser.tip", ToggleState.Enabled)
    assert(toggleSystem.enrolledToggles.forall(_.isEnabled))

    val memoryStorage = new MemoryStorageMedium
    memoryStorage.save(toggleSystem)

    toggleSystem.setStateOf("pw.bowser.tip", ToggleState.Disabled)
    assert(toggleSystem.enrolledToggles.forall(!_.isEnabled))

    memoryStorage.load(toggleSystem)
    assert(toggleSystem.enrolledToggles.forall(_.isEnabled))

  }

}
